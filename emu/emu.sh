#!/usr/bin/env bash
set -e

###############################################################################
# Project Structure
###############################################################################
EMU="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
ROOT="$( dirname $EMU )"
BUILD=$ROOT/build


###############################################################################
# Config
###############################################################################
source $ROOT/radian.conf.env
source $ROOT/util/log.sh
source $ROOT/util/download.sh

# Radian
RADIAN_IMG="radian_$RADIAN_VERSION.img"

# QEMU
QEMU_KERNEL="kernel-qemu-4.14.79-stretch"
QEMU_DTB="versatile-pb.dtb"
QEMU_REMOTE="https://github.com/dhruvvyas90/qemu-rpi-kernel/raw/master/"


###############################################################################
# Main
###############################################################################
download $EMU/$QEMU_KERNEL $QEMU_REMOTE/$QEMU_KERNEL
download $EMU/$QEMU_DTB $QEMU_REMOTE/$QEMU_DTB

log_task "Starting emulation..."
sudo qemu-system-arm \
  -kernel $EMU/$QEMU_KERNEL \
  -dtb $EMU/$QEMU_DTB \
  -hda $BUILD/$RADIAN_IMG \
  -m 256 -M versatilepb -cpu arm1176 \
  -serial stdio \
  -append "rw console=ttyAMA0 root=/dev/sda2 rootfstype=ext4  loglevel=8 rootwait fsck.repair=yes memtest=1" \
  -netdev user,id=mynet0,net=192.168.76.0/24,dhcpstart=192.168.76.9 \
  -no-reboot \
;
