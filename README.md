***r a d i a n***
==================================
***RaspberryPi Media Center***

# Development
libguestfs-tools


----
# Reference
### Modifying raw image file
 - https://raspberrypi.stackexchange.com/questions/855/is-it-possible-to-update-upgrade-and-install-software-before-flashing-an-image
 - https://www.raspberrypi.org/forums/viewtopic.php?t=190154
 - https://www.raspberrypi.org/forums/viewtopic.php?t=21632#p1160522
### Emulation
 - https://blog.agchapman.com/using-qemu-to-emulate-a-raspberry-pi/
 - https://github.com/wimvanderbauwhede/limited-systems/wiki/Raspbian-%22stretch%22-for-Raspberry-Pi-3-on-QEMU
