HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
source $HERE/log.sh

function download {
  filename=$1
  url=$2
  if [ ! -f "$ETC/$filename" ]; then
    log_task "Downloading $filename..."
    wget -O "$ETC/$filename" $url -q --show-progress
  else
    log_noop "Already have $filename."
  fi
}
export -f download
