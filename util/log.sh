# Something has begun.
function log_task {
  echo -e "\e[96m   ►   \e[1m$1\e[0m"
}
export -f log_task

# Something has already been done.
function log_noop {
  echo -e "\e[2m   -   $1\e[0m"
}
export -f log_noop

# Something succeeded, yay.
function log_ok {
  echo -e "\n\e[32m   ✔   \e[1m$1\e[0m\n"
}
export -f log_ok
