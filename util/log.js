export function logger(cat) {
  return (...msg) => console.log($`[ ${cat} ]`, ...msg);
}
