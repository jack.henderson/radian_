#!/usr/bin/env bash
set -e

###############################################################################
# Project Structure
###############################################################################
BUILD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
ROOT="$( dirname $BUILD )"
ETC=$BUILD/etc
TMP=$BUILD/tmp
MNT=/mnt/radian
SRC=$ROOT/src


###############################################################################
# Config
###############################################################################
source $ROOT/radian.conf.env
source $ROOT/util/log.sh
source $ROOT/util/download.sh

# Raspbian
RASP_FILENAME="2019-04-08-raspbian-stretch-lite"
RASP_IMG="$RASP_FILENAME.img"
RASP_ZIP="$RASP_FILENAME.zip"
RASP_REMOTE="https://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2019-04-09/$RASP_ZIP"

# Radian
RADIAN_IMG="radian_$RADIAN_VERSION.img"
RADIAN_BOOT_START=8192
RADIAN_BOOT_SECTORS=87851
RADIAN_FS_START=98304
RADIAN_FS_SECTORS=3424256
let "RADIAN_BOOT_SIZELIMIT = RADIAN_BOOT_SECTORS * 512"
let "RADIAN_BOOT_OFFSET = RADIAN_BOOT_START * 512"
let "RADIAN_FS_SIZELIMIT = RADIAN_FS_SECTORS * 512"
let "RADIAN_FS_OFFSET = RADIAN_FS_START * 512"


###############################################################################
# Utils
###############################################################################

# Run command chrooted into mounted image
function pi {
  sudo chroot $MNT $@
}


###############################################################################
# Main
###############################################################################

# Ensure build directories exist
if [ ! -d $ETC ]; then mkdir $ETC; fi
if [ -d $TMP ]; then rm -r $TMP; fi; mkdir $TMP
sudo mkdir -p $MNT

if [ ! -f $ETC/$RASP_IMG ]; then
  download $RASP_ZIP $RASP_REMOTE

  log_task "Extracting image..."
  unzip -d $ETC $ETC/$RASP_ZIP
else
  log_noop "Already have Raspbian image."
fi

log_task "Cloning Raspbian..."
cp $ETC/$RASP_IMG $TMP/$RADIAN_IMG

log_task "Collecting files..."
cp -r $SRC/pi $TMP/fs/
cp $ROOT/package*.json $TMP/fs/
cp $ROOT/radian.conf.env $TMP/fs/


log_task "Mounting..."
sudo mount -o loop,offset=$RADIAN_FS_OFFSET,sizelimit=$RADIAN_FS_SIZELIMIT $TMP/$RADIAN_IMG $MNT/
sudo mount -o loop,offset=$RADIAN_BOOT_OFFSET,sizelimit=$RADIAN_BOOT_SIZELIMIT $TMP/$RADIAN_IMG $MNT/boot
sudo mount --bind /dev $MNT/dev
sudo mount --bind /proc $MNT/proc

log_task "Copying files..."
sudo cp -r --update --verbose $TMP/fs/* $MNT
sudo cp /usr/bin/qemu-arm-static $MNT/usr/bin/

cd $MNT

log_task "Configuring apt..."
pi apt-get install dirmngr
pi apt-key adv --fetch-keys https://deb.nodesource.com/gpgkey/nodesource.gpg.key
pi apt-get update

log_task "Upgrading..."
# pi apt-get upgrade -y

log_task "Installing requirements..."
# apt
pi apt-get install -y $(cat radian/requirements.apt)
# NPM
pi npm config set unsafe-perm true
pi npm install --production --global
pi npm config set unsafe-perm false

log_task "Configuring system..."

log_task "Cleaning up image..."
pi rm /usr/bin/qemu-arm-static

cd $BUILD

log_task "Unmounting image..."
sudo umount $MNT/proc
sudo umount $MNT/dev
sudo umount $MNT/boot
sudo umount $MNT/
if [ -d $MNT ]; then sudo rm -r $MNT; fi


cp $TMP/$RADIAN_IMG $BUILD

if [ -d $TMP ]; then rm -r $TMP; fi

log_ok "Build complete!"
